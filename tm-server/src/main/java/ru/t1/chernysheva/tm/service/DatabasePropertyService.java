package ru.t1.chernysheva.tm.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.chernysheva.tm.api.service.IDatabasePropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public final class DatabasePropertyService implements IDatabasePropertyService {

    @NotNull
    private static final String DB_URL_DEFAULT = "jdbc:postgresql://localhost:5432/postgres?currentSchema";

    @NotNull
    private static final String DB_URL_KEY = "database.url";

    @NotNull
    private static final String DB_PASSWORD_DEFAULT = "21356";

    @NotNull
    private static final String DB_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DB_USER_DEFAULT = "postgres";

    @NotNull
    private static final String DB_USER_KEY = "database.username";

    @NotNull
    private static final String DB_SCHEMA_DEFAULT = "tm";

    @NotNull
    private static final String DB_SCHEMA_KEY = "database.schema";

    @NotNull
    private static final String DB_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private static final String DB_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DB_CACHE_DEFAULT = "false";

    @NotNull
    private static final String DB_CACHE_KEY = "database.l2Cache";

    @NotNull
    private static final String DB_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQL95Dialect";

    @NotNull
    private static final String DB_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DB_SHOW_SQL_DEFAULT = "false";

    @NotNull
    private static final String DB_SHOW_SQL_KEY = "database.showSQL";

    @NotNull
    private static final String DB_HBM2DDL_DEFAULT = "none";

    @NotNull
    private static final String DB_HBM2DDL_KEY = "database.hbm2ddl";

    @NotNull
    private static final String DB_CACHE_REGION_KEY = "database.cacheRegionClass";

    @NotNull
    private static final String DB_CACHE_REGION_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";

    @NotNull
    private static final String DB_QUERY_CACHE_KEY = "database.useQueryCache";

    @NotNull
    private static final String DB_QUERY_CACHE_DEFAULT = "true";

    @NotNull
    private static final String DB_MINIMAL_PUTS_KEY = "database.useMinPuts";

    @NotNull
    private static final String DB_MINIMAL_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_KEY = "database.regionPrefix";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String DB_CACHE_PROVIDER_KEY = "database.configFilePath";

    @NotNull
    private static final String DB_CACHE_PROVIDER_DEFAULT = "hazelcast.xml";
    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "db-conf.properties";

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "db-conf.properties";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    public DatabasePropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }


    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DB_DRIVER_KEY, DB_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DB_URL_KEY, DB_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DB_PASSWORD_KEY, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DB_USER_KEY, DB_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBSchema() {
        return getStringValue(DB_SCHEMA_KEY, DB_SCHEMA_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBL2Cache() {
        return getStringValue(DB_CACHE_KEY, DB_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DB_DIALECT_KEY, DB_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSQL() {
        return getStringValue(DB_SHOW_SQL_KEY, DB_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHbm2DDL() {
        return getStringValue(DB_HBM2DDL_KEY, DB_HBM2DDL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegion() {
        return getStringValue(DB_CACHE_REGION_KEY, DB_CACHE_REGION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBQueryCache() {
        return getStringValue(DB_QUERY_CACHE_KEY, DB_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBMinimalPuts() {
        return getStringValue(DB_MINIMAL_PUTS_KEY, DB_MINIMAL_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheRegionPrefix() {
        return getStringValue(DB_CACHE_REGION_PREFIX_KEY, DB_CACHE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBCacheProvider() {
        return getStringValue(DB_CACHE_PROVIDER_KEY, DB_CACHE_PROVIDER_DEFAULT);
    }

}
