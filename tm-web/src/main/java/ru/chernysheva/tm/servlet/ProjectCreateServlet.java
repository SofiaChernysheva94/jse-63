package ru.chernysheva.tm.servlet;

import ru.chernysheva.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/create/*")
public final class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp
    ) throws IOException {
        ProjectRepository.getInstance().create();
        resp.sendRedirect("/projects");
    }

}
