package ru.chernysheva.tm.model;

import ru.chernysheva.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractModel {

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateFinish;

    private String projectId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String projectId) {
        this.name = name;
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }


}
