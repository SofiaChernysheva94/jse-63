package ru.chernysheva.tm.repository;

import ru.chernysheva.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final  ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("ONE"));
        add(new Project("TWO"));
        add(new Project("THREE"));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis()));
    }

    public void deleteById(final String id) {
        projects.remove(id);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

}
